<?php
namespace yii2portal\access\backend\forms;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;

/**
 * Role form
 */
class RoleForm extends \yii2portal\core\backend\forms\Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $permissions = [];
    public $name = '';
    public $description = '';

    /**
     * @inheritdoc
     */
    public function rules()
    {


        $rules = [
            [['name'], 'required'],
            [['name'], 'unique',
                'targetClass' => "yii2portal\\access\\common\\models\\AuthItem",
                'filter' => function ($query) {
                    $query->andWhere([
                        'type' => Item::TYPE_ROLE
                    ]);
                }
                , 'on' => SCENARIO_CREATE],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'match', 'pattern' => "/[a-z0-9_-]+/i"],
            [['permissions'], 'safe'],
        ];

        /*if($this->isNewRecord){
            $rules[] = [['name'], 'unique',
                'filter' => function ($query) {
                    $query->andWhere([
                        'type'=>Item::TYPE_ROLE
                    ]);
                }
            ];
        }*/

        return $rules;
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'description', 'permissions'],
            self::SCENARIO_UPDATE => ['description', 'permissions'],
        ];
    }

    public function getAllPermissions(){
        return ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', function ($el) {
            return $el->description ? $el->description : Yii::t('yii2portal/access', $el->name);
        });
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('yii2portal/access', 'Name'),
            'description' => Yii::t('yii2portal/access', 'Description'),
            'permissions' => Yii::t('yii2portal/access', 'Permissions'),
        ];
    }

    public function __toString()
    {
        return $this->description;
    }
}
