<?php
namespace yii2portal\access\backend\forms;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;
use yii2portal\core\backend\forms\Subform;
use yii2portal\core\common\widgets\Widget;

/**
 * Role form
 */
class UserRoleForm extends Subform
{
    public $roles = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {


        $rules = [
            [['roles'], 'required'],
            [['roles'], 'each', 'rule'=>[
                'in', 'range' => ArrayHelper::getColumn(Yii::$app->authManager->getRoles(), 'name')
            ]],

        ];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roles' => Yii::t('yii2portal/access', 'Roles'),
        ];
    }


    public function subformId(){
        return "access-form";
    }

    public function subformName(){
        return Yii::t('yii2portal/access', 'Access');
    }

    public function loadDefault(){
        $this->roles = array_keys(Yii::$app->authManager->getRolesByUser($this->parent->id));
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::$app->authManager->revokeAll($this->parent->getId());
        foreach ($this->roles as $roleId) {
            $role = Yii::$app->authManager->getRole($roleId);
            if ($role) {
                Yii::$app->authManager->assign(
                    $role,
                    $this->parent->getId()
                );
            }
        }
        return true;
    }

    public function delete()
    {
        Yii::$app->authManager->revokeAll($this->parent->getId());
        return true;
    }


}
