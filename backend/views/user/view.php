<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h3><?= Yii::t('yii2portal/access', 'Roles of user'); ?> <?= $user->getUserName(); ?></h3>
<?php $form = ActiveForm::begin(); ?>

<?= Html::checkboxList('roles', $user_permit, $roles, ['separator' => '<br>']); ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('yii2portal/access', 'Update'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

