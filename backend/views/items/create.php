<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii2portal\access\common\models\AuthItem */

$this->title = Yii::t('yii2portal/access', 'Create Auth Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii2portal/access', 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
