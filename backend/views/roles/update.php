<?php

use yii\bootstrap\Html;
use yii2portal\core\backend\widgets\ActiveForm;


/* @var $this \yii\web\View */
/* @var $model RbacRole */
/* @var $form \yii\bootstrap\ActiveForm */

$this->title = Yii::t('yii2portal/access', 'Update role') . ": {$model->name}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii2portal/access', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="links-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['readonly' => true]) ?>
        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'permissions')->dualListBox($permissions, [
            'options' => [
                'size' => 20
            ]
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('yii2portal/access', 'Update'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>