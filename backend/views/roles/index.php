<?php

use common\models\User;
use yii\data\ArrayDataProvider;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('yii2portal/access', 'Roles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii2portal/access', 'Add role'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    $dataProvider = new ArrayDataProvider([
        'allModels' => Yii::$app->authManager->getRoles(),
        'sort' => [
            'attributes' => ['name', 'description'],
        ],
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => DataColumn::className(),
                'attribute' => 'name',
                'label' => Yii::t('yii2portal/access', 'Name'),
                'value' => function ($el) {
                    return Yii::t('yii2portal/access', $el->name);
                },
            ],
            [
                'class' => DataColumn::className(),
                'attribute' => 'description',
                'label' => Yii::t('yii2portal/access', 'Description')
            ],
            [
                'class' => DataColumn::className(),
                'label' => Yii::t('yii2portal/access', 'Allowed'),
                'format' => ['html'],
                'value' => function ($data) {
                    $permissions = Yii::$app->authManager->getPermissionsByRole($data->name);
                    return implode('<br>', ArrayHelper::map($permissions, 'name', function ($el) {
                        return $el->description ? $el->description : Yii::t('yii2portal/access', $el->name);
                    }));
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ]
    ]);
    ?>
</div>