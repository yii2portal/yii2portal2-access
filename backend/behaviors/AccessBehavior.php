<?php

namespace yii2portal\access\backend\behaviors;

use Yii;
use yii\base\Module;
use yii\behaviors\AttributeBehavior;
use yii\web\ForbiddenHttpException;

class AccessBehavior extends AttributeBehavior
{

    public $rules = [];
    public $redirect_url = false;
    public $login_url = false;

    private $_rules = [];

    public function events()
    {
        return [
            Module::EVENT_BEFORE_ACTION => 'interception',
        ];
    }

    /**
     * @param $event \yii\base\ActionEvent
     * @throws ForbiddenHttpException
     */
    public function interception($event)
    {
        if (!isset(Yii::$app->i18n->translations['yii2portal/access'])) {
            \yii2portal\core\backend\Module::registerTranslations('access');
        }


        $action = $event->action;
        $request = Yii::$app->getRequest();
        $route = $request->resolve();

        $this->createRule();
        $user = Yii::$app->user;


        if ($action instanceof \yii\base\InlineAction) {

            $route[0] = implode('/', [
                $action->controller->module->id,
                'backend',
                $action->controller->id,
                $action->controller->action->id,
            ]);
        }

        if (!($action instanceof \yii\web\ErrorAction) &&
            $action->controller->module->id != 'debug' &&
            $action->controller->module->id != 'gii'
        ) {
            if (!$this->cheсkByRule($action, $user, $request)) {

                if (!$this->checkPermission($route)) {
                    if (Yii::$app->user->isGuest && $this->login_url) {
                        Yii::$app->response->redirect($this->login_url)->send();
                        exit();
                    }
                    //Если задан $redirect_url
                    if ($this->redirect_url) {
                        Yii::$app->response->redirect($this->redirect_url)->send();
                        exit();
                    } else {
                        throw new ForbiddenHttpException(Yii::t('yii2portal/access', 'Permission denied'));
                    }
                }
            }
        }
    }

    protected function createRule()
    {
        foreach ($this->rules as $controller => $rule) {
            foreach ($rule as $singleRule) {
                if (is_array($singleRule)) {
                    $option = [
                        'controllers' => [$controller],
                        'class' => 'yii\filters\AccessRule'
                    ];
                    $this->_rules[] = Yii::createObject(array_merge($option, $singleRule));
                }
            }
        }
    }

    protected function cheсkByRule($action, $user, $request)
    {
        foreach ($this->_rules as $rule) {
            if ($rule->allows($action, $user, $request))
                return true;
        }
        return false;
    }

    protected function checkPermission($route)
    {
        $routePathTmp = explode('/', $route[0]);
        $routeVariant = array_shift($routePathTmp);

        if (Yii::$app->user->can($routeVariant, $route[1]))
            return true;

        foreach ($routePathTmp as $routePart) {
            $routeVariant .= '/' . $routePart;
            if (Yii::$app->user->can($routeVariant, $route[1])) {
                return true;
            }
        }
        return false;
    }
}