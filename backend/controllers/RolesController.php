<?php

namespace yii2portal\access\backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii2portal\access\backend\forms\RoleForm;
use yii2portal\core\backend\controllers\Controller;

/**
 * RolesController implements the CRUD actions for AuthItem model.
 */
class RolesController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new RoleForm([
            'scenario' => RoleForm::SCENARIO_CREATE
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {


            $role = Yii::$app->authManager->createRole($model->name);
            $role->description = $model->description;
            Yii::$app->authManager->add($role);
            foreach ($model->permissions as $permission) {
                Yii::$app->authManager->addChild(
                    $role,
                    Yii::$app->authManager->getPermission($permission)
                );
            }
            return $this->redirect(['index']);
        } else {
            return $this->render(
                'create',
                [
                    'permissions' => $model->getAllPermissions(),
                    'model' => $model,
                ]
            );
        }
    }

    public function actionUpdate($id)
    {
        $role = $this->findRole($id);

        $model = new RoleForm([
            'scenario' => RoleForm::SCENARIO_UPDATE
        ]);

        $model->name = $role->name;
        $model->description = $role->description;
        $model->permissions = array_keys(Yii::$app->authManager->getPermissionsByRole($id));

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $role->description = $model->description;
            Yii::$app->authManager->update($model->name, $role);
            Yii::$app->authManager->removeChildren($role);

            foreach ($model->permissions as $permission) {
                Yii::$app->authManager->addChild(
                    $role,
                    Yii::$app->authManager->getPermission($permission)
                );
            }
            return $this->redirect(['index']);
        } else {
            return $this->render(
                'update',
                [
                    'permissions' => $model->getAllPermissions(),
                    'model' => $model,
                ]
            );
        }
    }

    protected function findRole($id)
    {
        $role = Yii::$app->authManager->getRole($id);
        if ($role) {
            return $role;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelete($id)
    {
        $role = $this->findRole($id);
        Yii::$app->authManager->removeChildren($role);
        Yii::$app->authManager->remove($role);

        return $this->redirect(['index']);
    }

    protected function setPermissions($permissions, $role)
    {

    }
}
