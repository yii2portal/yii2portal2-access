<?php
namespace yii2portal\access\backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii2portal\core\backend\controllers\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii2portal\users\common\models\User;

class UserController extends Controller
{
    public $moduleName = 'permit';

    public function actionView($id)
    {
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        $user_permit = array_keys(Yii::$app->authManager->getRolesByUser($id));
        $user = $this->findUser($id);
        return $this->render('view', [
            'user' => $user,
            'roles' => $roles,
            'user_permit' => $user_permit,
            'moduleName' => Yii::$app->controller->module->id
        ]);
    }

    public function actionUpdate($id)
    {
        $user = $this->findUser($id);
        Yii::$app->authManager->revokeAll($user->getId());
        if(Yii::$app->request->post('roles')){
            foreach(Yii::$app->request->post('roles') as $role)
            {
                $new_role = Yii::$app->authManager->getRole($role);
                Yii::$app->authManager->assign($new_role, $user->getId());
            }
        }
        return $this->redirect(Url::to(["/".Yii::$app->controller->module->id."/user/view", 'id' => $user->getId()]));
    }

    private function findUser($id)
    {
        $user = User::findOne($id);
        if(!$user){
            throw new NotFoundHttpException(Yii::t('yii2portal/access', "User not found"));
        } else {
            return $user;
        }
    }
}