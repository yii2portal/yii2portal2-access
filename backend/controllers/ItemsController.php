<?php

namespace yii2portal\access\backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii2portal\access\backend\models\AuthItemSearch;
use yii2portal\access\common\models\AuthItem;
use yii2portal\core\backend\controllers\Controller;

/**
 * ItemsController implements the CRUD actions for AuthItem model.
 */
class ItemsController extends Controller
{

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Sync all actions in backend.
     * @return mixed
     */
    public function actionSync()
    {
        $auth = Yii::$app->authManager;
        $modules = Yii::$app->getModules();
        foreach ($modules as $moduleName => $moduleInstance) {
            $module = Yii::$app->getModule($moduleName);
            if ($module instanceof \yii2portal\core\backend\Module) {
                $files = scandir($module->controllerPath);
                $namespace = $module->controllerNamespace;
                foreach ($files as $file) {
                    if (substr($file, 0, 1) != '.') {
                        $controllerClass = str_replace('.php', '', $file);
                        $controllerName = str_replace('Controller', '', $controllerClass);
                        $controllerName = mb_strtolower(preg_replace("~([A-Z]{1})~", '-\\1', $controllerName));
                        if (substr($controllerName, 0, 1) == '-') {
                            $controllerName = substr_replace($controllerName, '', 0, 1);
                        }

                        $classMethods = get_class_methods("{$namespace}\\{$controllerClass}");
                        foreach ($classMethods as $method) {
                            if (substr($method, 0, 6) == 'action' && $method != 'actions') {
                                $actionName = substr_replace($method, '', 0, 6);
                                $actionName = mb_strtolower(preg_replace("~([A-Z]{1})~", '-\\1', $actionName));
                                if (substr($actionName, 0, 1) == '-') {
                                    $actionName = substr_replace($actionName, '', 0, 1);
                                }
                                $pemissionName = "{$moduleName}/backend/{$controllerName}/{$actionName}";
//                                var_dump($pemissionName, $method);

                                if (!$auth->getPermission($pemissionName)) {
                                    $permission = $auth->createPermission($pemissionName);
                                    $auth->add($permission);
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->setFlash(Yii::t('yii2portal/access', "All permissions are synced"));
        return $this->redirect(['index']);

    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem([
            'scenario' => AuthItem::SCENARIO_UPDATE
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = AuthItem::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id);
        $permission = Yii::$app->authManager->getPermission($id);
        Yii::$app->authManager->remove($permission);
        return $this->redirect(['index']);
    }
}
